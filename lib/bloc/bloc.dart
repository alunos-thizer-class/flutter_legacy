import 'package:flutter/material.dart';
import 'package:flutter_legacy/bloc/list_bloc.dart';
import 'package:flutter_legacy/bloc/model/item_bloc.dart';
import 'package:flutter_legacy/pages/layout.dart';

class BlocPage extends StatefulWidget {
  static String tag = '/bloc';

  @override
  _BlocPageState createState() => _BlocPageState();
}

class _BlocPageState extends State<BlocPage> {
  ListBloc _listBloc = ListBloc();

  @override
  dispose() {
    super.dispose();
    _listBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var content = StreamBuilder(
      stream: _listBloc.output,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasError) {
          throw "Error: ${snapshot.error}";
        }

        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return RefreshProgressIndicator();
            break;
          default:
            return GridView.count(
              crossAxisCount: 3,
              children: List.generate(
                snapshot.data.length,
                (index) {
                  ItemBloc containerItem = snapshot.data[index];

                  return Padding(
                    padding: EdgeInsets.all(5),
                    child: GestureDetector(
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        decoration: BoxDecoration(
                          color: containerItem.color,
                          borderRadius: containerItem.borderRadius,
                        ),
                        height: 70,
                        width: 70,
                      ),
                      onTap: () {
                        _listBloc.refreshOn(containerItem.id);
                      },
                    ),
                  );
                },
              ),
            );
        }
      },
    );

    return Layout.render(
      context,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            color: Colors.deepPurple,
            height: 100,
            child: Center(
              child: Text(
                'BLoC - Toque para animar',
                style: Theme.of(context).textTheme.headline.copyWith(color: Colors.white),
              ),
            ),
          ),
          Expanded(child: content)
        ],
      ),
    );
  }
}
