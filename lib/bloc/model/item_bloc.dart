import 'package:flutter/widgets.dart';

///
/// Apenas uma model para guardarmos as informacoes
/// de cada um dos itens da lista de modo mais consistente
class ItemBloc {
  int id;

  Color color;

  BorderRadius borderRadius;

  ItemBloc({this.id, this.color, this.borderRadius});
}
