import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_legacy/bloc/model/item_bloc.dart';

class ListBloc {
  /// Construtor
  /// Popula [items] pela primeira vez
  ListBloc() {
    // Borda arredondada aleatoria
    var radius = _rndRadius();

    // Cria uma lista com 9 posicoes
    // e adiciona a cada um deles um
    // item da model ItemBloc
    List.generate(9, (i) {
      items.add(
        ItemBloc(
          id: i,
          color: Colors.primaries[i],
          borderRadius: radius,
        ),
      );
    });

    // Adiciona a lista TODA ao Stream
    _input.add(items);
  }

  List<ItemBloc> items = [];

  StreamController<List<ItemBloc>> _controller = StreamController<List<ItemBloc>>();

  Stream<List<ItemBloc>> get output => _controller.stream;

  StreamSink<List<ItemBloc>> get _input => _controller.sink;

  refreshOn(int id) {
    // Define um mesmo arredondamento aleatorio para TODOS os itens
    var radius = _rndRadius();
    items.forEach((i) => i.borderRadius = radius);

    // Encontra o item alvo
    ItemBloc item = items.firstWhere((i) => i.id == id);
    item.color = _rndColor();

    // Adiciona a lista toda ao Stream
    _input.add(items);
  }

  /// Define uma cor aleatoria entre as primarias do Material
  _rndColor() {
    var newColorIndex = Random().nextInt(Colors.primaries.length);
    return Colors.primaries[newColorIndex];
  }

  // Define um arredondamento de borda aleatório
  _rndRadius() {
    var newBorderRadius = Random().nextInt(50);
    return BorderRadius.circular(newBorderRadius.toDouble());
  }

  // Elimina o stream
  // Indispensavel
  void dispose() {
    _controller.close();
  }
}
