import 'package:flutter/foundation.dart';
import 'package:flutter_legacy/provider/model/favorite_item.dart';

class ModelFavoriteList with ChangeNotifier {
  final List<ModelFavoriteItem> items = [];

  ModelFavoriteList();

  void add(ModelFavoriteItem item) {
    items.add(item);

    notifyListeners();
  }

  void removeById(int id) {
    items.removeWhere((i) => i.id == id);

    notifyListeners();
  }

  void removeByIndex(int index) {
    items.removeAt(index);

    notifyListeners();
  }

  ModelFavoriteItem getById(int id) {
    return items.firstWhere((i) => i.id == id, orElse: () => null);
  }

  int get count => items.length;
}
