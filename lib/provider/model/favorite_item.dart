import 'package:flutter/material.dart';

@immutable
class ModelFavoriteItem {
  final int id;

  final Color color;

  final String name;

  ModelFavoriteItem({this.id, this.color, this.name});
}
