import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_legacy/pages/layout.dart';

import 'package:flutter_legacy/provider/model/favorite_item.dart';
import 'package:flutter_legacy/provider/favorites.dart';
import 'package:flutter_legacy/provider/model/favorites.dart';

class ProviderPage extends StatelessWidget {
  static String tag = '/provider';

  @override
  Widget build(BuildContext context) {
    var favorites = Provider.of<ModelFavoriteList>(context);

    var itens = [
      {"color": Color(0xfffb6496), "nome": "Control Flow"},
      {"color": Color(0xffdd6496), "nome": "Canequinha"},
      {"color": Color(0xffda6496), "nome": "Caneta azul"},
      {"color": Color(0xffa96496), "nome": "MC Deejay"},
      {"color": Color(0xffa16496), "nome": "Lombardino"},
      {"color": Color(0xff946496), "nome": "Ligatuno tumba"},
      {"color": Color(0xff7e6496), "nome": "Alagarto tunao"},
      {"color": Color(0xff3e6496), "nome": "Amondaiba nadura"},
      {"color": Color(0xff286496), "nome": "Spaghemi loste"},
      {"color": Color(0xff086496), "nome": "Heepom domtow"},
    ];

    var content = ListView.builder(
      itemCount: itens.length,
      itemBuilder: (BuildContext context, int index) {
        // Recupera o item se ele estiver na lista
        var item = favorites.getById(index);

        return Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(10, (index == 0 ? 10 : 0), 10, 10),
          padding: EdgeInsets.only(top: 10, bottom: 10),
          child: ListTile(
            leading: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: itens[index]['color'],
              ),
              height: 50,
              width: 50,
            ),
            title: Text(itens[index]['nome']),
            trailing: IconButton(
              icon: Icon(
                (item == null) ? Icons.favorite_border : Icons.favorite,
                color: Colors.red[300],
              ),
              onPressed: () {
                if (item == null) {
                  favorites.add(
                    ModelFavoriteItem(
                      id: index,
                      name: itens[index]['nome'],
                      color: itens[index]['color'],
                    ),
                  );
                } else {
                  favorites.removeById(index);
                }
              },
            ),
          ),
        );
      },
    );

    return Layout.render(
      context,
      content: content,
      title: 'Provider Sample',
      appBarActions: <Widget>[
        Container(
          height: double.infinity,
          width: 60,
          color: Colors.blue[900],
          child: IconButton(
            icon: Icon(Icons.folder_special),
            onPressed: () {
              Navigator.of(context).pushNamed(ProviderFavoritesPage.tag);
            },
          ),
        ),
      ],
    );
  }
}
