import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_legacy/provider/model/favorites.dart';
import 'package:flutter_legacy/pages/layout.dart';

class ProviderFavoritesPage extends StatelessWidget {
  static String tag = '/states/provider/favorites';

  @override
  Widget build(BuildContext context) {
    var favorites = Provider.of<ModelFavoriteList>(context);

    var content = Column(
      children: <Widget>[
        Container(
          color: Colors.blue[900],
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Itens favoritados',
                  style: Theme.of(context).textTheme.headline.copyWith(color: Colors.white),
                ),
                Text(
                  'Total de ${favorites.count} itens',
                  style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.white),
                ),
              ],
            ),
          ),
          height: 75,
          width: double.infinity,
        ),
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: favorites.count,
            itemBuilder: (BuildContext context, int index) {
              var item = favorites.items[index];

              return Container(
                color: Colors.white,
                margin: EdgeInsets.fromLTRB(10, (index == 0 ? 10 : 0), 10, 10),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: ListTile(
                  leading: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: item.color,
                    ),
                    height: 50,
                    width: 50,
                  ),
                  title: Text(item.name),
                  trailing: IconButton(
                    icon: Icon(
                      Icons.favorite,
                      color: Colors.red[300],
                    ),
                    onPressed: () {
                      favorites.removeByIndex(index);
                    },
                  ),
                ),
              );
            },
          ),
        )
      ],
    );

    return Layout.render(
      context,
      content: content,
      title: 'Provider Sample',
    );
  }
}
