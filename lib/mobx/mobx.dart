import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_legacy/mobx/controller.dart';
import 'package:flutter_legacy/pages/layout.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class MobxPage extends StatelessWidget {
  static String tag = '/mobx';

  @override
  Widget build(BuildContext context) {
    var mobxController = MobxController();

    var content = Container(
      child: GridView.count(
        crossAxisCount: 3,
        children: List.generate(9, (i) {
          return GestureDetector(
            child: Container(
              margin: EdgeInsets.all(3),
              color: Colors.white,
              height: double.infinity,
              width: double.infinity,
              child: Center(
                child: Observer(builder: (BuildContext context) {
                  var item = mobxController.getItemById(i);

                  var itemColor = Colors.black;
                  if (item != null) {
                    itemColor = (item.val == 'X') ? Colors.green : Colors.blue;
                  }

                  return Text(
                    (item == null) ? '?' : item.val,
                    style: Theme.of(context).textTheme.title.copyWith(
                          fontSize: 58,
                          color: itemColor,
                        ),
                  );
                }),
              ),
            ),
            onTap: () {
              mobxController.reveal.call([i, TokenKey.xis]);
            },
          );
        }),
      ),
    );

    return Layout.render(
      context,
      content: Column(
        children: <Widget>[
          Container(
            color: Colors.pink[900],
            height: 100,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'MobX - Toque para animar',
                    style: Theme.of(context).textTheme.headline.copyWith(color: Colors.white),
                  ),
                  Observer(builder: (BuildContext context) {
                    return Text(
                      mobxController.alert.value,
                      style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.grey[200]),
                    );
                  }),
                ],
              ),
            ),
          ),
          Expanded(child: content),
          FlatButton(
            color: Colors.indigo,
            onPressed: () => mobxController.restart.call(),
            child: Text(
              'Restart',
              style: Theme.of(context).textTheme.title.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
