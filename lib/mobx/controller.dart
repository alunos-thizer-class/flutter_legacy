import 'dart:async';

import 'package:flutter_legacy/mobx/model/item_mobx.dart';
import 'package:mobx/mobx.dart';

// Torna quase impossivel digitar errado
enum TokenKey { xis, oh }

class MobxController {
  MobxController() {
    // Define as acoes do controller
    reveal = Action(_reveal);
    restart = Action(_restart);
    showAlert = Action(_showAlert);
  }

  // Lista de itens ja selecionados
  final ObservableList<ItemMobx> items = ObservableList<ItemMobx>();

  // Alertas mostrados para o usuario
  final Observable<String> alert = Observable('');

  Action reveal;

  /// O [index] eh a posicao dele na lista, comecando de
  /// zero.
  ///
  /// Atraves deste metodo o usuario escolhe um item.
  /// Se o 'usuario' um ser humano o [tokenKey] sera X.
  ///
  /// Apos 2seg do usuario humano escolher o item,
  /// este metodo dispara para si mesmo o [tokenKey] como
  /// O (letra)
  _reveal(int index, TokenKey tokenKey) {
    // Se o item 'index' ainda nao existe na lista
    // de selecionados
    if (getItemById(index) == null) {
      // Adiciona este item
      items.add(ItemMobx(
        id: index,
        val: _charByToken(tokenKey),
      ));

      // Quando value eh 'X'?
      // Quando foi um ser humano que escolheu...
      if (tokenKey == TokenKey.xis) {
        //
        // Cria uma nova lista apenas com as possiveis
        // posicoes que ainda nao foram selecionadas
        var available = Iterable<int>.generate(9).toList();
        items.forEach((i) {
          available.remove(i.id);
        });

        // Embaralha a lista e retorna o primeiro
        // isso faz com que a maquina escolha um
        // elemento de maneira aleatoria
        var nextRnd = (available..shuffle()).first;

        // Apos 2 segundos da jogada do humano, a
        // maquina lanca sua escolha tambem
        Future.delayed(Duration(seconds: 2), () {
          reveal.call([nextRnd, TokenKey.oh]);
        });
      }

      // Que tal adicionar uma logica para verificar
      // se alguem ja ganhou? ;)

      // Limpa possiveis alertas anteriores
      showAlert.call(['']);
    } else {
      // Item ja foi escolhido, lanca mensagem
      showAlert.call(['Escolha outro...']);
    }
  }

  Action restart;

  /// Remove todos os itens selecionados da lista
  /// e mostra mensagem para comecar novamente
  _restart() {
    items.clear();

    showAlert.call(['Comece novamente']);
  }

  Action showAlert;

  /// Modifica o texto mostrado como "mensagem"
  /// na tela
  _showAlert(String content) {
    alert.value = content;
  }

  String _charByToken(TokenKey tokenKey) {
    return tokenKey == TokenKey.xis ? 'X' : 'O';
  }

  /// Se existir, retorna o item [id] da lista
  /// de selecionados
  ItemMobx getItemById(int id) {
    return items.firstWhere((i) => i.id == id, orElse: () => null);
  }
}
