import 'package:flutter/material.dart';
import 'package:flutter_legacy/bloc/bloc.dart';
import 'package:flutter_legacy/mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'package:flutter_legacy/pages/home.dart';

import 'package:flutter_legacy/provider/model/favorites.dart';
import 'package:flutter_legacy/provider/provider.dart';
import 'package:flutter_legacy/provider/favorites.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ListenableProvider<ModelFavoriteList>(create: (context) => ModelFavoriteList()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: HomePage.tag,
        routes: {
          HomePage.tag: (context) => HomePage(title: 'Flutter Demo Home Page'),
          ProviderPage.tag: (context) => ProviderPage(),
          ProviderFavoritesPage.tag: (context) => ProviderFavoritesPage(),
          BlocPage.tag: (context) => BlocPage(),
          MobxPage.tag: (context) => MobxPage(),
        },
      ),
    );
  }
}
