import 'package:flutter/material.dart';
import 'package:flutter_legacy/bloc/bloc.dart';
import 'package:flutter_legacy/mobx/mobx.dart';
import 'package:flutter_legacy/pages/home.dart';
import 'package:flutter_legacy/provider/provider.dart';
import 'package:meta/meta.dart';

class Layout {
  static Widget render(
    BuildContext context, {
    @required Widget content,
    String title = 'Demo App',
    Widget floatingActionButton,
    List<Widget> appBarActions,
  }) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: appBarActions,
      ),
      body: content,
      backgroundColor: Color(0xffeeeeee),
      floatingActionButton: floatingActionButton,
      drawer: Container(
        width: MediaQuery.of(context).size.width * .7,
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.fromLTRB(10, 30, 10, 10),
          children: <Widget>[
            Text(
              'Menu de navegação',
              style: Theme.of(context).textTheme.title,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(HomePage.tag);
              },
              child: Container(
                child: Text('Home Page'),
                padding: EdgeInsets.only(top: 15),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ProviderPage.tag);
              },
              child: Container(
                child: Text('Provider'),
                padding: EdgeInsets.only(top: 15),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(BlocPage.tag);
              },
              child: Container(
                child: Text('BLoC'),
                padding: EdgeInsets.only(top: 15),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(MobxPage.tag);
              },
              child: Container(
                child: Text('MobX'),
                padding: EdgeInsets.only(top: 15),
              ),
            )
          ],
        ),
      ),
    );
  }
}
